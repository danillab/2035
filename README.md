## Run

http://localhost:8000/add_file/{URL}

```bash
docker-compose build
docker-compose up
docker-compose up kafka_daemon
```

Local add task
```bash
python utils/add_kafka.py
open http://localhost:8000/add_file/https://raw.githubusercontent.com/wurstmeister/kafka-docker/master/docker-compose.yml
```
