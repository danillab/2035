from aiokafka import AIOKafkaConsumer
import asyncio
import json
import requests
import time

# KAFKA_URL='localhost:9092'
KAFKA_URL='kafka:29092' # for in docker
WEB_URL = 'http://web:8000/get_file/'


loop = asyncio.get_event_loop()

async def consume():
    consumer = AIOKafkaConsumer('for_download', loop=loop, bootstrap_servers=KAFKA_URL)

    
    await consumer.start()

    print('consumer started')

    try:
        # Consume messages
        async for msg in consumer:
            print("consumed: ", msg.topic, msg.partition, msg.offset, msg.key, msg.value, msg.timestamp)

            value = msg.value.decode('utf-8')
            # try:
            data = json.loads(value)
            # except:
            #     print('Bad json string', value)
            #     continue

            if data.get('action')=='download' and data.get('source') and data.get('url'):

                response = requests.get(WEB_URL+data.get('url'))

                print('send code', response.status_code)
            else:
                print('Bad format', value)


    finally:
        # Will leave consumer group; perform autocommit if enabled.
        await consumer.stop()

loop.run_until_complete(consume())