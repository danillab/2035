from django.db.models import F
from celery import shared_task
from django.conf import settings
from kafka import KafkaProducer
import json
import requests
from pathlib import Path
from random import randint


@shared_task
def download_file(url):
    response = requests.get(url)
    if response.status_code != 200:
        return False

    path = Path(BASE_DIR, randint(1000000000000000000, 9999999999999999999))
    
    with open(path, 'wb') as f:
        f.write(response.content)
        
    producer = KafkaProducer(bootstrap_servers=settings.KAFKA_URL)
    mes = {'action':'downloaded', 'path': path.absolute(), 'url': url}
    producer.send('downloads', json.dumps(mes).encode('utf-8'))
