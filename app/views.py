import json
from kafka import KafkaProducer
from django.http import Http404
from django.http import JsonResponse
from django.conf import settings
from .tasks import download_file

def index(request):
    return JsonResponse({'work': 1})

def add_file(request, url):
    producer = KafkaProducer(bootstrap_servers=settings.KAFKA_URL)
    mes = {'action':'download', 'source': 'application1', 'url': url}
    producer.send('for_download', json.dumps(mes).encode('utf-8'))
    return JsonResponse({'status': 1})

def get_file(request, url):
    download_file.delay(url)
    return JsonResponse({'status': 1})