from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('add_file/<path:url>', views.add_file),
    path('get_file/<path:url>', views.get_file),
]