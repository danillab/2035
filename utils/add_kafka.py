
# bootstrap_servers='kafka:29092' # for in docker
bootstrap_servers='localhost:9092'

import json
from kafka import KafkaProducer

producer = KafkaProducer(bootstrap_servers=bootstrap_servers)
producer.send('for_download', json.dumps({'action': 'download', 'source': 'application1', 'url': 'https://bit.ly/2F5nCiI'}).encode('utf-8'))


# from kafka import KafkaConsumer
# consumer = KafkaConsumer('for_download', bootstrap_servers='localhost:9092')
# for msg in consumer:
#     print(msg.value)
